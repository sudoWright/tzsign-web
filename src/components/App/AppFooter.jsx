import React, { useMemo } from 'react';
import styled from 'styled-components';

const TheFooter = styled.footer`
  display: flex;
  justify-content: space-between;
  padding: 12px 46px;
  font-size: 14px;
  font-weight: 300;

  @media (${({ theme }) => theme.smDown}) {
    padding: 15px;
  }
`;

const AppFooter = () => {
  const currentYear = useMemo(() => {
    return new Date().getFullYear();
  }, []);

  return (
    <TheFooter>
      <div>
        © {currentYear} |{' '}
        <a
          href="https://gitlab.com/tezos-paris-hub/tzsign/tzsign-api"
          target="_blank"
          rel="noreferrer"
        >
          Back-end repo
        </a>{' '}
        |{' '}
        <a
          href="https://gitlab.com/tezos-paris-hub/tzsign/tzsign-web"
          target="_blank"
          rel="noreferrer"
        >
          Front-end repo
        </a>{' '}
        |{' '}
        <a
          href={`${process.env.REACT_APP_API_URL}/docs`}
          target="_blank"
          rel="noreferrer"
        >
          API docs
        </a>
      </div>
      <div>
        Thanks to <a href="https://atticlab.net/">Attic Lab</a>
        {/* {' '} |  <a href="">Terms</a> |{' '} */}
        {/* <a href="">Licences</a> |{' '} */}
        {/* <a href="">Github</a> |{' '} */}
        {/* <a href="">Cookie Policy</a> | Version 0.0.1 */}
      </div>
    </TheFooter>
  );
};

export default AppFooter;
