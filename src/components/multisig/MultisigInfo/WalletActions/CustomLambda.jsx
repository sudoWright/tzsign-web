import React from 'react';
import PropTypes from 'prop-types';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { Button, Form as BForm, InputGroup } from 'react-bootstrap';
import { FormLabel, FormSubmit } from '../../../styled/Forms';
import useAPI from '../../../../hooks/useApi';
import { useOperationsDispatchContext } from '../../../../store/operationsContext';
import { handleError } from '../../../../utils/errorsHandler';
import { useContractStateContext } from '../../../../store/contractContext';
import useRpcAPI from '../../../../hooks/useRpcApi';

const CustomLambda = ({ onCreate, onCancel }) => {
  const { createOperation } = useAPI();
  // const { assets } = useAssetsStateContext();
  const { contractAddress } = useContractStateContext();
  const { setOps } = useOperationsDispatchContext();
  const { normalizeLambda } = useRpcAPI();

  // eslint-disable-next-line camelcase
  const customLambda = async ({ lambda }, resetForm) => {
    try {
      const normalizedLambda = await normalizeLambda(lambda);
      const payload = {
        contract_id: contractAddress,
        type: 'custom',
        custom_payload: JSON.stringify(normalizedLambda.data.normalized),
      };

      // eslint-disable-next-line no-unreachable
      const newTx = await createOperation(payload);
      await setOps((prev) => {
        return [newTx.data, ...prev];
      });

      resetForm();
      onCreate();
    } catch (e) {
      handleError(e);
    }
  };

  return (
    <Formik
      initialValues={{ custom_payload: '' }}
      enableReinitialize
      //   validationSchema={Yup.lazy((values) =>
      //     schema(
      //       convertAssetSubunitToAssetAmount(1, values.asset.scale),
      //       values.asset.ticker,
      //     ),
      //   )}
      onSubmit={async (values, { resetForm }) => {
        await customLambda(values, resetForm);
      }}
    >
      {({ isSubmitting, errors, touched, handleChange }) => (
        <Form>
          <BForm.Group controlId="lambda" style={{ marginBottom: 0 }}>
            <FormLabel>Submit a custom lambda</FormLabel>
            <InputGroup>
              <Field
                as={BForm.Control}
                type="text"
                name="lambda"
                aria-label="lambda"
                placeholder=""
                autoComplete="off"
                isInvalid={!!errors.lambda && touched.lambda}
                isValid={!errors.lambda && touched.lambda}
                onChange={(value) => {
                  handleChange(value);
                  // setAlias('');
                }}
              />
              {/* <InputGroup.Append> */}
              {/*  <SelectCustom */}
              {/*    options={bakers} */}
              {/*    displayValue={false} */}
              {/*    onChange={(value) => { */}
              {/*      setFieldValue('baker', value.value); */}
              {/*      setAlias(value.label); */}
              {/*    }} */}
              {/*  /> */}
              {/* </InputGroup.Append> */}

              <ErrorMessage
                name="lambda"
                component={BForm.Control.Feedback}
                type="invalid"
              />
            </InputGroup>

            {/* <BForm.Text> */}
            {/*  <TextAccent>{alias}</TextAccent> */}
            {/* </BForm.Text> */}
          </BForm.Group>

          <FormSubmit>
            <Button
              variant="danger"
              style={{ marginRight: '10px' }}
              onClick={onCancel}
            >
              Cancel
            </Button>

            <Button type="submit" disabled={isSubmitting}>
              Submit
            </Button>
          </FormSubmit>
        </Form>
      )}
    </Formik>
  );
};

CustomLambda.propTypes = {
  onCreate: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default CustomLambda;
