import axios from 'axios';

const useAPI = () => {
  const API = axios.create({
    baseURL: process.env.REACT_APP_RPC_URL,
    timeout: 30000,
    headers: {
      'Content-type': 'application/json',
    },
    // withCredentials: false,
    // responseType: 'json',
  });

  const normalizeLambda = (lambda) => {
    return API.post(`/chains/main/blocks/head/helpers/scripts/normalize_data`, {
      data: JSON.parse(lambda),
      type: {
        prim: 'lambda',
        args: [
          { prim: 'unit' },
          { prim: 'list', args: [{ prim: 'operation' }] },
        ],
      },
      unparsing_mode: 'Optimized',
      legacy: false,
    });
  };

  return {
    normalizeLambda,
  };
};

export default useAPI;
